# Blazar Voyage

## What have we been doing so far?
The kickoff was an event where we set the expectations for the Voyage, including the schedule, expected activities, and desired outcomes.

We created a basic Value Stream Map to help understand the flow that leads to delivering a story.  This led to a discussion on how stories are created and understood.  We learned that currently:

- Each feature is broken into stories of about one to two days in effort
- These stories are aligned based on UI or Backend, and are fairly explicit in implementation details
- Team members are fairly specialized between UI and Backend, but are very willing to try the Other Side.

### Initial Activities
The primary building blocks of the Voyage are **daily mobbing sessions**, **weekly workshops**, and **20% Learning Time**  This is essentially the Samman Style, with the exception that the Samman style generally involves a daily Learning Hour, which would replace the 20% learning time.

- Daily Mobbing Sessions, also called Ensemble, are two hour blocks of implementing existing stories following the strong style mobbing. Much of the effort here is to introduce good test driven practices, reduce coupling and learn new refactoring techniques.
- Weekly workshops are aimed at accomplishing a single goal, identifies as something that will lead us toward solving a specific problem.
    - This portion of the voyage we are focusing on increasing the frequency of releases.

## How's it going?
We are seeing some growing interest in the technical aspects. We also have been able to get the team to swarm around solving a specific problem.  In this case the problem was how to make a better release experience for our Product Owner.
On the down side, we are not getting much traction in the mobbing, and a general suspicion that refactoring to better design is not **getting things done**.  There have been as many mob sessions canceled as have been attended.

None of this is too terribly surprising, as we are right now deep in the 'trough of resistance'.  There are several challenges to be addressed, and the key is to not try to address them all at the same time.  The issues can be boiled down into technical and procedural:
#### Technical Issues
1. The team is very comfortable with copy and pasting existing code, which is causing a considerable amount of duplicated code.  This is probably the most common thing teams do these days, and convincing them to take the extra time to write something new is coupled with the procedural, or cultural, issue of needing to go as fast as possible.
2. The tools that the team has access to are limited.  As they move forward they will need to be able to take advantage of modern tools like ORM, Containers, and possibly Feature Flags


#### Procedural Issues
1. There is a strong undercurrent of "we need to work as fast as possible".  This is of course common, but needs to be overcome.  
2. The team has had a lot of change thrust upon them all at once.  They have been re-organized, they have embarked on this journey, and they are moving away from AOS all at the same time.  This is way too much to just absorb overnight.
3. The AOS system has serious flaws, and creates a highly segmented, top-down structure that is most likely not the initial intent.  While the team is moving away from this, the hangover will last for a litte while longer

## Next Steps
In order to get the most out of the Voyage, accepting that due to the reorganization this voyage might get cut short, we need to focus on small changes that can make a big impact.  

### Mobbing to an Outcome
For our next set of mobbing sessions, we will focus on delivering a completed story well.  This means maybe not doing every possible refactoring, but starting to get into the habit of refactoring as a matter of course.  Test Driven will focus on ensuring our tests lead to design, even if that means they are still more tightly coupled than we would prefer.

### Workshops
The workshops have so far been focused on getting better Acceptance Criteria.  This has been very succesful.  We now should pivot into how we use these AC as tests, preferably automated. Inreasing the automation of the pipeline will enhance this effort.

### Blitzes, or 20% Learning
The two dedicated Blitz structure seems to have thrown the team for a loop.  We should look into a different approach, perhaps one day per week, or some other approach like "learning hours".


## Journals?
>#### 4/18/2024 
 >Today we joined a short code kata session.  Since there were very few people here, we discussed some ideas about code in general.  We also experimented with "code with me", which is a different approach to mobbing, similar to grouop editing a google doc. Steve will be out on Friday, and mobbing sessions are usually very light on Fridays, especially Fridays at the end of the pay period.  Either Nicholas or Bryan will be guiding whoever does join.
    
>#### 4/22/2024
>    We worked on two modules today:  One was the email notifiation and the other was enabling and disabling checklists.  We discovered some useful tests, and also learned a bit about some of the challenges with using ReactJS.  Steve forgot to do the sparrow decks, so we will be getting back to those in our next seession.

>#### 4/23/2024
>Mobbing was canceled in order to allow for a team retrospective.

>#### 4/25/2024
>We mobbed today on the next story, around deativating Categories and their children.  This led to a discussion on how to best design what feels like a straight pass through, which in its own right led to a request for more "this is what good looks like" examples.  We also performed the long method sparrow decks.
 
>#### 4/26/2024
>Today was a workshop day.  We looked into the best ways to start bringing containers over from cloud to on-premises in an automated fashion. Some good discussion among several team members, but still a very quiet interaction.  We did some work in establishing a service account and pulling the appropriate containers.

>#### 5/1/2024
 >Missed some updates from Friday due to travel.  Today we worked on the 
 service 
 for activating and deactivating Categories and their children.  We did some just straight mobbing, and also did some refactoring.  We explored:
> - Extracting a method
> - Introducing a builder for large classes
> - formatting for easier reading

>#### 5/2/2024
>Workshop day!  Because webex was down, we weren't able to focus on the pipeline changes we had been.  Instead, we moved to a discussion of how we might be able to implement some of our work in Spring instead of Vertx.


>#### 6/25/2024 We havent been updating the log for a while...quick summary of the past month: 
 >- Working on the hypothesis that more frequent deliveries will be enhanced and enabled by moving core services from Reactive Java to .net.
  >- The team has been struggling mightily with getting a pipeline to run all the way in ADO.  There are many issues that have taken considerable time and effort to overcome, but today we seem to have reached success.
 >- Ensemble work has become pretty smooth.  We have been paying closer attention to utilizng common patterns and avoiding duplicated code.
 >- Duplication and Long Methods are still our biggst code smells, by far.  While we are gatting much better at writing tests and also doing some basic refactoring, copy and paste is still the primary mechanism of development.  I believe this will become more clear, and easier to overcome, when we move to the .net implementation.  
 >- Reactive Java is a functional skin on top of java, and wbile the team is very talented, they don't really know how it works, and is mostly copying what has acted as a "template" and applying it to each and every service request.  This "gets the job done", but is not conducive to well written, highly readable code.
 >- We will be focusing on making it easier to deploy frequently, preferably continuously, via automated pipelines and eventually also feature toggles.

>#### 6/26/2024
>Today we worked on the UI side of a story for exporting to excel.  We focused on trying to do this test-driven, as opposed to test-after.  We discovered that the nature of xls export utilities made mocking the export less than friendly.

>#### 6/27/2024
>Today's Workshop covered a lot of ground.  We created an automation pipeline that will reduce nearly all human intervention to get a released deployed to Dev on premise. Our next step will be to remove the remaining human steps.
>
> We also were able to create a step in our pipeline to report each deployment to doradash!