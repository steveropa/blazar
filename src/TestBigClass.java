import org.junit.Assert;
import org.junit.Test;

public class TestBigClass {

    @Test
    public void getFirstIntegerShouldReturnIntegerAsSet() {
        BigClass mybigclass = new BigClassBuilder()
                .setFirstinteger(1)
                .setSecondinteger(2)
                .setMoreInterestingStuff("more interesting stuff")
                .createBigClass();
        mybigclass.setFirstInteger(2);
        Assert.assertEquals(2, mybigclass.getFirstInteger());
    }

    @Test
    public void BigClassFirstIntegerGetsInitialized() {
        BigClass mybigclass = new BigClassBuilder()
                .setFirstinteger(1)
                .setSecondinteger(2)
                .setMoreInterestingStuff("more interesting stuff")
                .createBigClass();
        Assert.assertEquals(1, mybigclass.getFirstInteger());
    }

    @Test
    public void BigClassSecondIntegerGetsInitialized() {
        BigClass mybigclass = new BigClassBuilder()
                .setFirstinteger(1)
                .setSecondinteger(2)
                .setMoreInterestingStuff("more interesting stuff")
                .createBigClass();
        Assert.assertEquals(2, mybigclass.getSecondInteger());
    }

    @Test
    public void BigClassInterestingTextGetsInitialized() {
        BigClass mybigclass = new BigClassBuilder()
                .setFirstinteger(1)
                .setSecondinteger(2)
                .setMoreInterestingStuff("Interesting stuff")
                .createBigClass();
        Assert.assertEquals("Interesting stuff", mybigclass.getInterestingText());
    }

    @Test
    public void BigClassGetsInitializedWithNewText() {
        BigClass mybigclass = new BigClassBuilder()
                .setFirstinteger(1)
                .setSecondinteger(2)
                .setMoreInterestingStuff("more interesting stuff")
                .createBigClass();
        Assert.assertEquals("more interesting stuff", mybigclass.getInterestingText());
    }


}
