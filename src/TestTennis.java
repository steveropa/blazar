
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;



public class TestTennis {

    public static final String FIFTEEN_LOVE = "Fifteen - Love";

    @Test
    public void NewGameShouldReturnLoveAll(){
        TennisGame game = new TennisGame();

        String score = game.WhatsTheScore();

        Assertions.assertEquals("Love All", score);
    }

    @Test
    public void OnePersonScoresShouldReturnLFifteenLove(){
        var game = new TennisGame();
        var score = game.WhatsTheScore();

        game.playerOneScores();

        Assertions.assertEquals(FIFTEEN_LOVE, score);
    }

    @Test
    public void PersonTwoScoredFirstShouldReturnLoveFifteen(){

    }

}
