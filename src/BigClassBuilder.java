public class BigClassBuilder {
    private int firstinteger = 0;
    private int secondinteger = 1;
    private String moreInterestingStuff = "im interesting";
    private String mostinterestingtext = "i most interesting";

    public BigClassBuilder setFirstinteger(int firstinteger) {
        this.firstinteger = firstinteger;
        return this;
    }

    public BigClassBuilder setSecondinteger(int secondinteger) {
        this.secondinteger = secondinteger;
        return this;
    }

    public BigClassBuilder setMoreInterestingStuff(String moreInterestingStuff) {
        this.moreInterestingStuff = moreInterestingStuff;
        return this;
    }

    public BigClassBuilder setMostinterestingtext(String mostinterestingtext) {
        this.mostinterestingtext = mostinterestingtext;
        return this;
    }

    public BigClass createBigClass() {
        return new BigClass(firstinteger, secondinteger, moreInterestingStuff, mostinterestingtext);
    }
}