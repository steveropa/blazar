public class BigClass {
    private int firstinteger;
    private int secondinteger;
    private String interestingtext;
    private String mostinterestingtext;

    public BigClass(int firstinteger, int secondinteger, String moreInterestingStuff,String mostinterestingtext) {
        this.firstinteger = firstinteger;
        this.secondinteger = secondinteger;
        this.interestingtext = moreInterestingStuff;
        this.mostinterestingtext = mostinterestingtext;
    }

    public int getFirstInteger() {
        return firstinteger;
    }

    public void setFirstInteger(int firstInteger) {
        this.firstinteger = firstInteger;
    }

    public void setSecondInteger(int secondInteger) {
        this.secondinteger = secondInteger;
    }


    public int getSecondInteger() {

        return secondinteger;
    }

    public String getInterestingText() {
        return interestingtext;
    }
}
